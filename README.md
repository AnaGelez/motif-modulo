Ce petit programme a été écrit pour [cet atelier](https://zestedesavoir.com/forums/sujet/5334/creer-un-motif-en-utilisant-le-modulo/).

Il permet de générer des motifs en fonction d'un modulo et d'un facteur.

# Exemples

![table de 64 modulo 97](modulo-64-97.png)

![table de 68 modulo 7](modulo-68-7.png)

![table de 68 modulo 87](modulo-68-87.png)

[Une vidéo de Micmaths qui explique tout ça](https://www.youtube.com/watch?v=-X49VQgi86E)

# Compilation

Il faut avoir Vala 0.20 minimum avec Cairo et Gdk (livrés avec Vala). Ensuite clonez le projet, rendez vous dans le dossier où se trouve le 
code, et tapez :

```
valac --pkg cairo --pkg gdk-3.0 main.vala Image.vala -o modulo
```

# Utilisation

La commande s'utilise ainsi :

```
modulo <table> <modulo> [r] [g] [b] [--no-text] [output]
```

*Les paramètres entre crochets sont optionels.*

r, g et b déterminent la couleur du motif.