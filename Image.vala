/*
 Code pour l'atelier https://zestedesavoir.com/forums/sujet/5334/creer-un-motif-en-utilisant-le-modulo/
 Par Bat'
 Le 9 février 2016
 Licence GPL

------------
 Classe gérant l'affichage
*/

using Cairo;
using Gdk;

public class Image {

    // Constantes
    private static const int SIZE = 2000;

    private static const double RADIUS = SIZE / 3;

    // Propriétés
    private static ImageSurface surface {get; set;}

    private static Context ctx {get; set;}

    public static Point[] points {get; set;}

    public static void draw (int table, int modulo, int r = 0, int g = 0, int b = 0, bool no_text = false, string output = "blay") {

        // Initialisation
        surface = new ImageSurface (Format.ARGB32, SIZE, SIZE);
        ctx = new Cairo.Context (surface);

        // Calcul des points
        init_points (modulo);

        // Paramétrage du contexte
        ctx.set_line_width (1.0);
        ctx.set_source_rgba (r, g, b, 1);
        ctx.set_font_size (25.0);

        // dessin du cercle
        ctx.arc ((double)SIZE / 2.0, (double)SIZE / 2.0, RADIUS, 0.0, 360.0);
        ctx.stroke ();

        // numérotation des points
        if (!no_text) {
            int i = 0;
            foreach (Point pt in points) {
                ctx.set_source_rgba (0, 0, 0, 1);

                ctx.move_to (pt.x, pt.y);
                ctx.show_text (i.to_string());
                i++;
            }
            ctx.fill ();
        }

        // Dessin des lignes
        draw_lines (table, modulo);

        // rendu
        render (output + ".png");
    }

    public static void draw_lines (int table, int modulo) {
        print ("table %d modulo and %d", table, modulo);
        for (int i = 0; i < modulo; i++) {
            int start = i % modulo;
            int end = (i * table) % modulo;

            print ("\nstart : \n \tx : %d \n\ty : %d\nend : \n\tx : %d \n\ty : %d \n", points[start].x, points[start].y, points[end].x, points[end].y);

            ctx.move_to (points[start].x, points[start].y);
            ctx.line_to (points[end].x, points[end].y);
        }
        ctx.stroke ();
        print ("draw lines OK");

    }

    public static void init_points (int modulo) {

        points = new Point[modulo];
        print ("%d && %d \n", points.length, modulo);

        int center = SIZE / 2;

        for (int i = 0; i < modulo + 1; i++) {

            double deg_angle = (360.0 / (double)modulo) * (double)i;
            double angle = deg_angle * (Math.PI / 180);

            print ("%d - deg %g rad %g \n", i, deg_angle, angle);

            double x = RADIUS * Math.cos(angle) + center;
            double y = RADIUS * Math.sin(angle) + center;

            Point pt = Point () {
                x = (int)x,
                y = (int)y
            };

            points[i] = pt;
        }

        print ("init points OK");
    }

    public static void render (string file_name) {
        surface.write_to_png (file_name);
        print ("render OK");
    }

}
