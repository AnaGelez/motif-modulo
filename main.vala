/*
 Code pour l'atelier https://zestedesavoir.com/forums/sujet/5334/creer-un-motif-en-utilisant-le-modulo/
 Par Bat'
 Le 9 février 2016
 Licence GPL

------------
 Point d'entrée
*/

int main (string[] args) {

    if (args.length < 3) {
        print ("Erreur : argument maquant. \n Utilisation de la commande : %s <table> <modulo> [r] [g] [b] [--no-text] [output]", args[0]);
        return 1;
    }

    // Arguments
    int table = int.parse (args[1]);
    int modulo = int.parse (args[2]);
    int r = int.parse (args[3]);
    int g = int.parse (args[4]);
    int b =int.parse (args[5]);
    bool no_text = false;
    string output = "modulo-" + args[1] + "-" + args[2];

    if (args[7] != null) {
        output = args[7];
    }

    if (args[6] == "--no-text") {
        no_text = true;
    }

    Image.draw (table, modulo, r, g, b, no_text, output);
    return 0;
}
